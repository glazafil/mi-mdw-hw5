package order_processor;

import java.util.Hashtable;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import db.*;
import config.Config;

public class OrderProcessor implements MessageListener {
 
    // connection factory
    private QueueConnectionFactory qconFactory;
 
    // connection to a queue
    private QueueConnection qcon;
 
    // session within a connection
    private QueueSession qsession;
 
    // queue receiver that receives a message to the queue
    private QueueReceiver qreceiver;
 
    // queue where the message will be sent to
    private Queue queue;
    
 // queue sender that sends a message to the queue
    private QueueSender qsender;

    // a message that will be sent to the queue
    private ObjectMessage msg;
 
    // callback when the message exist in the queue
    public void onMessage(Message msg) {
        try {
            String msgText;
            if (msg instanceof TextMessage) {
                msgText = ((TextMessage) msg).getText();
            } else {
                msgText = msg.toString();
            }
            System.out.println("Message Received: " + msgText);
            
            String queueName;
            Order newMsg;
            Order obj = ((Order) ((ObjectMessage) msg).getObject());
         
            String type = ((Order) ((ObjectMessage) msg).getObject()).getType();
            
           if (type.equals("booking")) {
                queueName = "jms/mdw-hw3-bookings-queue";
                newMsg = new BookingOrder(obj.getData(), obj.getId()) ;
            } else if(type.equals("trip")) {
            	queueName = "jms/mdw-hw3-trips-queue";
            	newMsg = new TripOrder(obj.getData(), obj.getId());
            }
            else {
            	System.out.println("Unknown message!");
            	return;
            } 
            
            sendFarAway(queueName, newMsg);
            
        } catch (JMSException jmse) {
            System.err.println("An exception occurred: " + jmse.getMessage());
        } catch (Exception e) {
        	 e.printStackTrace();
		}
    }
 
    // create a connection to the WLS using a JNDI context
    public void init(Context ctx, String queueName)
            throws NamingException, JMSException {
 
        qconFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        qcon = qconFactory.createQueueConnection();
        qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        queue = (Queue) ctx.lookup(queueName);
 
        qreceiver = qsession.createReceiver(queue);
        qreceiver.setMessageListener(this);
 
        qcon.start();
    }
 
    // close sender, connection and the session
    public void closeFarAway() throws JMSException {
        qreceiver.close();
        qsession.close();
        qcon.close();
    }
 
    // start receiving messages from the queue
    public void receive(String queueName) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);
 
        InitialContext ic = new InitialContext(env);
 
        init(ic, queueName);
 
        System.out.println("Connected to " + queue.toString() + ", receiving messages...");
        try {
            synchronized (this) {
                while (true) {
                    this.wait();
                }
            }
        } finally {
            close();
            System.out.println("Finished.");
        }
    }
 
    public void initFarAway(Context ctx, String queueName)
            throws NamingException, JMSException {
 
            // create connection factory based on JNDI and a connection
            qconFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
            qcon = qconFactory.createQueueConnection();
 
            // create a session within a connection
            qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
 
            // lookups the queue using the JNDI context
            queue = (Queue) ctx.lookup(queueName);
 
            // create sender and message
            qsender = qsession.createSender(queue);
            msg = qsession.createObjectMessage();
        }
 
        // close sender, connection and the session
        public void close() throws JMSException {
            qsender.close();
            qsession.close();
            qcon.close();
        }
    
    public void sendFarAway(String queueName, Order message) throws Exception {
   	 
        // create a JNDI context to lookup JNDI objects (connection factory and queue)
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        System.out.println("SEnding");
        
        InitialContext ic = new InitialContext(env);
        initFarAway(ic, queueName);

        // send the message and close
        try {
            msg.setObject(message);
            qsender.send(msg, DeliveryMode.PERSISTENT, 8, 0);
            System.out.println("The message was sent to the destination " +
                    qsender.getDestination().toString());
        } finally {
            //closeFarAway();
        }
    }
    
    public static void main(String[] args) throws Exception {
        // input arguments
        String queueName = "jms/mdw-hw3-all-orders-queue";
 
        // create the producer object and receive the message
        OrderProcessor consumer = new OrderProcessor();
        consumer.receive(queueName);
        
    }
}
