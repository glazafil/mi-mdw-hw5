package db;

import java.io.Serializable;

public class Order implements Serializable {

	private static final long serialVersionUID = -6681761064712545800L;
	private String data = "";
	protected String type = "order";
	private int id;
	
	public Order(String data, int id) {
		this.data = data;
		this.id = id;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	
	public String getData() {
		return this.data;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String toString() {
		return "ORDER\nID: " + getId() + "\nDATA: " + getData() + "\nTYPE: " + getType() + "\n";
	}
	
	public String getType() {
		return this.type;
	}
}
